PYTHON_FILES := *.py
PORT := 8080

.DEFAULT_GOAL := api

.PHONY: setup
setup:
	pip3 install --user pipenv
	pipenv install


.PHONY: setup-dev
setup-dev: setup
	pipenv install --dev
	# if you receive an error like
	# RuntimeError: failed to find interpreter for Builtin discover of python_spec='python3.6m'
	# try editing .venv/lib/python3.6/site-packages/pre_commit/languages:make_venv
	# insert
	# python = "$(pwd)/.venv/bin/python3.6m" (evaluate pwd manually)
	# before the line
	# cmd = (sys.executable, '-mvirtualenv', envdir, '-p', python)
	pipenv run pre-commit install --install-hooks


.PHONY: fmt
fmt:
	pipenv run isort --recursive $(PYTHON_FILES)
	pipenv run black $(PYTHON_FILES)


.PHONY: test
test:
	pipenv run pre-commit run --all-files


.PHONY: demo
demo:
	pipenv run python3 demo_camera.py


.PHONY: api
api:
	FLASK_APP=api.py FLASK_DEBUG=true FLASK_ENV=development pipenv run flask run --host=0.0.0.0 --port=$(PORT)
