import datetime
from enum import Enum
from pathlib import Path

import cv2
import numpy as np
import pyrealsense2 as rs
from flask import Flask, Response, jsonify, send_file


app = Flask(__name__)

# Configure depth and color streams
pipeline = rs.pipeline()
config = rs.config()
config.enable_stream(rs.stream.depth, 640, 480, rs.format.z16, 30)
config.enable_stream(rs.stream.color, 640, 480, rs.format.bgr8, 30)

# Start streaming
pipeline.start(config)

root_dir = Path("data")
FILE_EXTENSION = "png"


class Mode(Enum):
    COLOR = "color"
    DEPTH = "depth"


@app.route("/health")
def health() -> Response:
    return jsonify(dict(status="ok"))


@app.route("/images")
def list_images_ids() -> Response:
    ids = [p.stem for p in root_dir.glob(f"*.{FILE_EXTENSION}")]
    ids = [id[: id.rfind("_")] for id in ids]
    ids = list(set(ids))
    ids = sorted(ids)
    ids = ids[::-1]
    return jsonify(ids)


@app.route("/take-picture")
def take_picture() -> Response:
    n_tries = 0
    color_frame, depth_frame, current_datetime = None, None, None
    while color_frame is None or depth_frame is None:
        if n_tries > 0:
            print(f"Retrying {n_tries}...")
        frames = pipeline.wait_for_frames()
        color_frame = frames.get_color_frame()
        depth_frame = frames.get_depth_frame()
        current_datetime = datetime.datetime.now()
        n_tries += 1

    image_id = current_datetime.isoformat().replace(":", ".")
    root_dir.mkdir(parents=True, exist_ok=True)
    for frame, mode, convert_fucntion in [
        (color_frame, Mode.COLOR, _identity),
        (depth_frame, Mode.DEPTH, _convert_depth_image),
    ]:
        image = convert_fucntion(np.asanyarray(frame.get_data()))
        filename = _get_filename(image_id, mode)
        cv2.imwrite(str(filename), image)

    return jsonify(dict(status="ok", n_tries=n_tries, image_id=image_id))


@app.route("/images/<string:image_id>/color")
def get_color_image(image_id: str) -> Response:
    return _get_image(image_id, Mode.COLOR)


@app.route("/images/<string:image_id>/depth")
def get_depth_image(image_id: str) -> Response:
    return _get_image(image_id, Mode.DEPTH)


def _get_image(image_id: str, mode: Mode) -> Response:
    filename = _get_filename(image_id, mode)
    return send_file(filename, mimetype=f"image/{FILE_EXTENSION}")


def _get_filename(image_id: str, mode: Mode) -> Path:
    return root_dir / f"{image_id}_{mode.value}.{FILE_EXTENSION}"


def _identity(image: np.array) -> np.array:
    return image


def _convert_depth_image(image: np.array) -> np.array:
    return cv2.convertScaleAbs(image, alpha=0.03)
