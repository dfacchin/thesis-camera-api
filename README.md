# Camera API

This repo exposes the Realsense D345 over a REST API with flask.
The corresponding android app will trigger taking photos and receive them for manual inspection.

## Setup
If you use [pipenv](https://pipenv.pypa.io/en/latest/) you can just run `make setup` or `make setup-dev` in case you want to develop.
Otherwise, you can install the python dependencies from `requirements.txt`.

## Camera demo
To test the python access to the camera, run `make demo`.

## API
To run the API, run `make api`. If you want to change the port, you can do `make PORT=XXXX api`.
When the code changes, the API reloads.
This causes the camera module to not properly be reloaded and then an error about the device being busy is shown.
To solve this, simply restart the `make api` command.
This should not be an issue when being run in production since the code does not change.
